/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#4d4d4d", /* black   */
	[1] = "#c70031", /* red     */
	[2] = "#29cf13", /* green   */
	[3] = "#d8e30e", /* yellow  */
	[4] = "#3449d1", /* blue    */
	[5] = "#8400ff", /* magenta */
	[6] = "#0798ab", /* cyan    */
	[7] = "#e2d1e3", /* white   */

	/* 8 bright colors */
	[8]  = "#5a5a5a", /* black   */
	[9]  = "#f01578", /* red     */
	[10] = "#6ce05c", /* green   */
	[11] = "#f3f79e", /* yellow  */
	[12] = "#97a4f7", /* blue    */
	[13] = "#c495f0", /* magenta */
	[14] = "#68f2e0", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#0e100a", /* background */
	[257] = "#f7f7f7", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
