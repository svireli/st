/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#1f1f1f", /* black   */
	[1] = "#f81118", /* red     */
	[2] = "#2dc55e", /* green   */
	[3] = "#ecba0f", /* yellow  */
	[4] = "#2a84d2", /* blue    */
	[5] = "#4e5ab7", /* magenta */
	[6] = "#1081d6", /* cyan    */
	[7] = "#d6dbe5", /* white   */

	/* 8 bright colors */
	[8]  = "#d6dbe5", /* black   */
	[9]  = "#de352e", /* red     */
	[10] = "#1dd361", /* green   */
	[11] = "#f3bd09", /* yellow  */
	[12] = "#1081d6", /* blue    */
	[13] = "#5350b9", /* magenta */
	[14] = "#0f7ddb", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#131313", /* background */
	[257] = "#d6dbe5", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
