/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {

	/* 8 normal colors */
	//[0] = "#000000", [> black   <]
	[0] = "#2a2a2a", /* black   */
	[1] = "#ff0000", /* red     */
	[2] = "#79ff0f", /* green   */
	[3] = "#d3bf00", /* yellow  */
	[4] = "#396bd7", /* blue    */
	[5] = "#b449be", /* magenta */
	[6] = "#66ccff", /* cyan    */
	[7] = "#bbbbbb", /* white   */

	/* 8 bright colors */
	[8]  = "#666666", /* black   */
	[9]  = "#ff0080", /* red     */
	[10] = "#66ff66", /* green   */
	[11] = "#f3d64e", /* yellow  */
	[12] = "#709aed", /* blue    */
	[13] = "#db67e6", /* magenta */
	[14] = "#7adff2", /* cyan    */
	[15] = "#ffffff", /* white   */

	/* special colors */
	[256] = "#000000", /* background */
	[257] = "#f2f2f2", /* foreground */
};

/*
 *  * Default colors (colorname index)
 *   * foreground, background, cursor
 *    */
unsigned int defaultfg = 257;
unsigned int defaultbg = 256;
static unsigned int defaultcs = 257;

/*
 *  * Colors used, when the specific fg == defaultfg. So in reverse mode this
 *   * will reverse too. Another logic would only make the simple feature too
 *    * complex.
 *     */
static unsigned int defaultitalic = 7;
static unsigned int defaultunderline = 7;
